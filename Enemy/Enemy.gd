extends KinematicBody

export var gravity = 10
export var speed = 200
export var damage = 5

onready var timer = $Hitbox/Timer

var space_state
var target
var wander_direction = Vector3(0, 0, 1)
var max_wander_time = 5.0
var wander_timer = 0.0
var insidebox = false



func _ready():
	space_state = get_world().direct_space_state
	

func _process(delta):
	if target:
		# If a target is set, follow it
		var result = space_state.intersect_ray(global_transform.origin, target.global_transform.origin)
		if result.collider.is_in_group("Player"):
			look_at(target.global_transform.origin, Vector3.UP)
			set_color_red()
			move_to_target(delta)
		else:
			set_color_green()
			target = null
	else:
		# If there's no target, wander around
		wander_timer -= delta
		if wander_timer <= 0:
			wander_timer = max_wander_time
			wander_direction = Vector3(rand_range(-1, 1), 0, rand_range(-1, 1))
		
		move_and_slide(wander_direction * speed * delta, Vector3.UP)
		look_at(global_transform.origin + wander_direction, Vector3.UP)
		move_and_slide(Vector3.DOWN * gravity * delta * 50)

func _on_Area_body_entered(body):
	if body.is_in_group("Player"):
		target = body
		print(body.name + " entered")
		set_color_red()

func _on_Area_body_exited(body):
	if body.is_in_group("Player"):
		target = null
		print(body.name + " exited")
		set_color_green()

func move_to_target(delta):
	var direction = (target.transform.origin - transform.origin).normalized()
	move_and_slide(direction * speed * delta, Vector3.UP)
	look_at(target.global_transform.origin, Vector3.UP)

func set_color_red():
	$MeshInstance.get_surface_material(0).set_albedo(Color(1, 0, 0))

func set_color_green():
	$MeshInstance.get_surface_material(0).set_albedo(Color(0, 1, 0))


func _on_Hitbox_body_entered(body):
	if body.is_in_group("Player"):
		print(body.name + " hit")
		insidebox = true
		Global.currenthealth = Global.currenthealth - damage
		timer.wait_time = 2
		timer.start()
	if body.is_in_group("Enemies"):
		pass

func _on_Hitbox_body_exited(body):
	if body.is_in_group("Player"):
		insidebox = false
		timer.stop()
	if body.is_in_group("Enemies"):
		pass

func _on_Timer_timeout():

	Global.currenthealth = Global.currenthealth - damage

