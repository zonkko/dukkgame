extends KinematicBody

export var speed = 10
export var acceleration = 5
export var gravity = 0.98
export var jump_power = 30
export var mouse_sensitivity = 0.3
export var MenuScene : PackedScene

const ACCEL_DEFAULT = 10
const ACCEL_AIR = 10

onready var head = $Head
onready var camera = $Head/Camera

var snap
var gravity_vec = Vector3()
var velocity = Vector3()
var camera_x_rotation = 0

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)


func _input(event):
	if event is InputEventMouseMotion:
		head.rotate_y(deg2rad(-event.relative.x * mouse_sensitivity))
		
		var x_delta = event.relative.y * mouse_sensitivity
		if camera_x_rotation + x_delta > -90 and camera_x_rotation + x_delta < 90:
			camera.rotate_x(deg2rad(-x_delta)) 
			camera_x_rotation += x_delta

# warning-ignore:unused_argument
#func _process(delta):
	#if Input.is_action_just_pressed("ui_cancel"):
		#get_tree().change_scene(MenuScene.resource_path)

func _physics_process(delta):
	var head_basis = head.get_global_transform().basis
	
	
	var direction = Vector3()
	if Input.is_action_pressed("move_forward"):
		direction -= head_basis.z
	elif Input.is_action_pressed("move_backward"):
		direction += head_basis.z
	
	if Input.is_action_pressed("running"):
		speed = 20
	else:
		speed = 10
	
	if Input.is_action_pressed("move_left"):
		direction -= head_basis.x
	elif Input.is_action_pressed("move_right"):
		direction += head_basis.x
	
	direction = direction.normalized()
	
	if is_on_floor():
		snap = -get_floor_normal()
		acceleration = ACCEL_DEFAULT
		gravity_vec = Vector3.ZERO
	else:
		snap = Vector3.DOWN
		acceleration = ACCEL_AIR
		gravity_vec += Vector3.DOWN * gravity * delta
	
	if Input.is_action_just_pressed("jump") and is_on_floor():
		velocity.y += jump_power
	
	velocity = move_and_slide(velocity, Vector3.UP)
