extends Label

export var lostGameScene : PackedScene

func _process(_delta):
	self.text = ("Health:" + str(Global.currenthealth) + "/" + str(Global.playerhealth))


	if Global.currenthealth <= 0:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		get_tree().change_scene(lostGameScene.resource_path)
