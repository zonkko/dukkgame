extends Control

export var MainGameScene : PackedScene

func _on_StartGame_button_up():
	
	get_tree().change_scene(MainGameScene.resource_path)


func _on_QuitButton_button_up():
	get_tree().quit()
