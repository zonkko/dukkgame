extends Label
var score = Global.score
var winscore = Global.maxscore
export var wonGameScene : PackedScene

func _process(_delta):
	self.text = ("score:" + str(Global.score) + "/" + str(Global.maxscore))

	if Global.score >= Global.maxscore:
		print("win")
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		get_tree().change_scene(wonGameScene.resource_path)
