extends Spatial

# Import the timer node and the enemy scene as a packed scene
onready var timer = $Timer # Create a timer node that will be used to spawn enemies
export (PackedScene) var Enemy # Export the enemy scene as a packed scene so that it can be referenced from other scenes
export var seconds_between = 2 # Export the number of seconds between each enemy spawn
export var enemy_num = 3 # Export the number of enemies to spawn
export var maxrange = 10
func _ready():
	timer.wait_time = seconds_between # Set the time between each timeout of the timer
	timer.start() # Start the timer
	print('timer started') # Print to the console to indicate that the timer has started

# Function called each time the timer times out
func _on_Timer_timeout():
	var enemy = Enemy.instance() # Create an instance of the enemy scene
	var scene_root = get_parent() # Get the root of the current scene
	scene_root.add_child(enemy) # Add the enemy instance as a child of the scene root
	
	# Set the position of the enemy instance randomly within the scene, making sure that the y position is set to ground level
	var ray_from = Vector3(rand_range(-maxrange, maxrange), 100, rand_range(-maxrange, maxrange))
	var ray_to = Vector3(rand_range(-maxrange, maxrange), -100, rand_range(-maxrange, maxrange))
	var result = get_world().direct_space_state.intersect_ray(ray_from, ray_to)
	enemy.transform.origin = result.position + Vector3(0, 0.5, 0)
	print ('enemy spawned') # Print to the console to indicate that an enemy has spawned
